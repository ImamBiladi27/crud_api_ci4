<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class RestApiPegawaiController extends ResourceController
{
    protected $modelName = 'App\Models\PegawaiModel';
    protected $format = 'json';
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        //
        $data = [
            'message' => 'success',
            'data_pegawai' => $this->model->findAll()
        ];
        return $this->respond($data, 200);
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        //

    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        //
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        //
        $this->model->insert([
            'nama' => esc($this->request->getVar('nama')),
            'jenis_kelamin' => esc($this->request->getVar('jenis_kelamin')),
            'no_telp' => esc($this->request->getVar('no_telp')),
            'email' => esc($this->request->getVar('email')),
            'alamat' => esc($this->request->getVar('alamat'))
        ]);
        $response = [
            'message' => 'Data pegawai berhasil ditambahkan'
        ];
        return $this->respondCreated($response);
    }

    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
        //
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        //
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        //
    }
}
